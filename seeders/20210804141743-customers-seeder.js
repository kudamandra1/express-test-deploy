"use strict";
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const dataCustomers = [];
    const dataAddress = [];
    for (let i = 0; i < 10; i++) {
      dataAddress.push({
        name: faker.address.streetSuffix(),
        createdAt: new Date(),
        updatedAt: new Date(),
      });
      dataCustomers.push({
        name: faker.name.findName(),
        addressId: i + 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }

    await queryInterface.bulkInsert("addresses", dataAddress);
    await queryInterface.bulkInsert("customers", dataCustomers);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
