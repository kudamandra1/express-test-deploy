const app = require("./providers/app");
require("dotenv").config();
const mongoose = require("mongoose");

mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .catch(function (error) {
    console.log(error.message);
  });

app.listen(process.env.PORT, function () {
  console.log(`running on port ${process.env.PORT}`);
});
