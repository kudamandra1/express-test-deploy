const { Router } = require("express");

module.exports = (controller) => {
  const router = Router();
  console.log(controller);
  router.get("/:name", controller.insertProduct);
  return router;
};
