const { Router } = require("express");

module.exports = function (controller) {
  const router = Router();

  router.get("/", controller.fetchCustomers);

  return router;
};
