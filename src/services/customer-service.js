module.exports = ({ customers, address }) => ({
  getAllCustomers: async function () {
    try {
      const dataCustomers = await customers.findAll({
        includes: { model: address },
      });
      return dataCustomers;
    } catch (error) {
      throw error;
    }
  },
});
