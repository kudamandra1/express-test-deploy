module.exports = (service) => ({
  fetchCustomers: async function (req, res) {
    await service
      .getAllCustomers()
      .then(function (data) {
        res.json(data);
      })
      .catch(function (error) {
        console.log(error);
        return res.status(500).send("Internal Server Error");
      });
  },
});
