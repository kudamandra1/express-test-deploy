//Init database
const db = require("../../models");
const products = require("../../schema/product");

//Init all Service
const customerService = require("../services/customer-service")(db);
const productService = require("../services/product-service")(products);

//Init all controller
const customerController = require("../controllers/customer-controller")(
  customerService
);
const productController = require("../controllers/product-controller")(
  productService
);

//Init all Router
const customerRouter = require("../routers/customer-router")(
  customerController
);
const productRouter = require("../routers/product-router")(productController);

module.exports = {
  customerRouter,
  productRouter,
};
