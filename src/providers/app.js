const express = require("express");
const container = require("./container");

const app = express();

app.get("/", (req, res) => res.send("welcome to index page"));
app.use("/api/customers", container.customerRouter);
app.use("/api/products", container.productRouter);

module.exports = app;
