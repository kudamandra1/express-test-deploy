const app = require("../../src/providers/app");
const request = require("supertest");

describe("testing app module", function () {
  test("route index page must return 200", async function () {
    return request(app).get("/").expect(200);
  });
});
